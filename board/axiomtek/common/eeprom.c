/*
 * Copyright 2018 Axiomtek Co., Ltd.
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <common.h>
#include <i2c.h>
#ifdef CONFIG_DM_I2C
#include <dm.h>
#endif

#ifndef CONFIG_SYS_I2C_EEPROM_ADDR
#define CONFIG_SYS_I2C_EEPROM_ADDR	0x50
#endif

#define CONFIG_SYS_I2C_EEPROM_ADDR_LEN  1

#ifndef CONFIG_SYS_I2C_EEPROM_BUS
#define CONFIG_SYS_I2C_EEPROM_BUS	0
#endif

#ifndef MAC_ADDR_OFFSET
#define MAC_ADDR_OFFSET			0
#endif

#ifdef CONFIG_DM_I2C
static struct udevice *g_dev = NULL;
#endif

#ifdef CONFIG_DM_I2C
static int ax_eeprom_init(void)
{
	int i2c_bus = CONFIG_SYS_I2C_EEPROM_BUS;
	uint8_t chip = CONFIG_SYS_I2C_EEPROM_ADDR;

	struct udevice *bus, *dev;
	int ret;

	if (!g_dev) {
		ret = uclass_get_device_by_seq(UCLASS_I2C, i2c_bus, &bus);
		if (ret) {
			printf("%s: No bus %d\n", __func__, i2c_bus);
			return ret;
		}

		ret = dm_i2c_probe(bus, chip, 0, &dev);
		if (ret) {
			printf("%s: Can't find device id=0x%x, on bus %d\n",
				 __func__, chip, i2c_bus);
			return ret;
		}

		/* Init */
		g_dev = dev;
	}
	return 0;
}

static int ax_eeprom_read(uint eeprom_bus, uint offset, uchar *buf, int len)
{
	int res;

	res = ax_eeprom_init();
	if (res < 0)
		return res;

	res = dm_i2c_read(g_dev, offset, buf, len);

	return res;
}
#else
static int ax_eeprom_read(uint eeprom_bus, uint offset, uchar *buf, int len)
{
	int res;
	unsigned int current_i2c_bus = i2c_get_bus_num();

	res = i2c_set_bus_num(eeprom_bus);
	if (res < 0)
		return res;

	res = i2c_read(CONFIG_SYS_I2C_EEPROM_ADDR, offset,
			CONFIG_SYS_I2C_EEPROM_ADDR_LEN, buf, len);

	i2c_set_bus_num(current_i2c_bus);

	return res;
}
#endif

/*
 * Routine: eeprom_read_mac_addr
 * Description: read mac address and store it in buf.
 */
int eeprom_read_mac_addr(uchar *buf)
{
	uint offset;

	offset = MAC_ADDR_OFFSET;

	return ax_eeprom_read(CONFIG_SYS_I2C_EEPROM_BUS, offset, buf, 6);
}

/*
 * Routine: eeprom_read_data
 * Description: read data and store it in buf.
 */
int eeprom_read_data(uchar *buf, uint offset, int len)
{
	return ax_eeprom_read(CONFIG_SYS_I2C_EEPROM_BUS, offset, buf, len);
}

/*
 * Copyright 2018 Axiomtek Co., Ltd.
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#ifndef _EEPROM_
#define _EEPROM_

#if defined(CONFIG_SYS_I2C) || defined(CONFIG_DM_I2C)
int eeprom_read_mac_addr(uchar *buf);
int eeprom_read_data(uchar *buf, uint offset, int len);
#else
static inline int eeprom_read_mac_addr(uchar *buf)
{
	return 1;
}

static inline int eeprom_read_data(uchar *buf, uint offset, int len)
{
	return 1;
}
#endif

#endif

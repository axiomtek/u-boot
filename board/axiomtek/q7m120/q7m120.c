/*
 * Copyright (C) 2019 Axiomtek Co., Ltd.
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <asm/arch/clock.h>
#include <asm/arch/imx-regs.h>
#include <asm/arch/iomux.h>
#include <asm/arch/mx6-pins.h>
#include <asm/mach-imx/spi.h>
#include <linux/errno.h>
#include <asm/gpio.h>
#include <asm/mach-imx/mxc_i2c.h>
#include <asm/mach-imx/iomux-v3.h>
#include <asm/mach-imx/boot_mode.h>
#include <asm/mach-imx/video.h>
#include <mmc.h>
#include <fsl_esdhc.h>
#include <miiphy.h>
#include <netdev.h>
#include <asm/arch/mxc_hdmi.h>
#include <asm/arch/crm_regs.h>
#include <asm/io.h>
#include <asm/arch/sys_proto.h>
#include <i2c.h>
#include <power/pmic.h>
#include <power/pfuze100_pmic.h>
#include <usb.h>
#include <usb/ehci-ci.h>
#include "../common/eeprom.h"
#ifdef CONFIG_SATA
#include <asm/mach-imx/sata.h>
#endif


DECLARE_GLOBAL_DATA_PTR;

#define UART_PAD_CTRL  (PAD_CTL_PUS_100K_UP |			\
	PAD_CTL_SPEED_MED | PAD_CTL_DSE_40ohm |			\
	PAD_CTL_SRE_FAST  | PAD_CTL_HYS)

#define USDHC_PAD_CTRL (PAD_CTL_PUS_47K_UP |			\
	PAD_CTL_SPEED_LOW | PAD_CTL_DSE_80ohm |			\
	PAD_CTL_SRE_FAST  | PAD_CTL_HYS)

#define ENET_PAD_CTRL  (PAD_CTL_PUS_100K_UP |			\
	PAD_CTL_SPEED_MED | PAD_CTL_DSE_40ohm | PAD_CTL_HYS)

#define SPI_PAD_CTRL (PAD_CTL_HYS | PAD_CTL_SPEED_MED | \
		      PAD_CTL_DSE_40ohm | PAD_CTL_SRE_FAST)

#define I2C_PAD_CTRL  (PAD_CTL_PUS_100K_UP |			\
	PAD_CTL_SPEED_MED | PAD_CTL_DSE_40ohm | PAD_CTL_HYS |	\
	PAD_CTL_ODE | PAD_CTL_SRE_FAST)

#define I2C_PMIC	1

#define I2C_PAD MUX_PAD_CTRL(I2C_PAD_CTRL)


#define KEY_VOL_UP	IMX_GPIO_NR(1, 11)

int dram_init(void)
{
	gd->ram_size = imx_ddr_size();
	return 0;
}

static iomux_v3_cfg_t const uart1_pads[] = {
	IOMUX_PADS(PAD_CSI0_DAT10__UART1_TX_DATA | MUX_PAD_CTRL(UART_PAD_CTRL)),
	IOMUX_PADS(PAD_CSI0_DAT11__UART1_RX_DATA | MUX_PAD_CTRL(UART_PAD_CTRL)),
};

static iomux_v3_cfg_t const enet_pads[] = {
	IOMUX_PADS(PAD_ENET_MDIO__ENET_MDIO	| MUX_PAD_CTRL(ENET_PAD_CTRL)),
	IOMUX_PADS(PAD_ENET_MDC__ENET_MDC	| MUX_PAD_CTRL(ENET_PAD_CTRL)),
	IOMUX_PADS(PAD_RGMII_TXC__RGMII_TXC	| MUX_PAD_CTRL(ENET_PAD_CTRL)),
	IOMUX_PADS(PAD_RGMII_TD0__RGMII_TD0	| MUX_PAD_CTRL(ENET_PAD_CTRL)),
	IOMUX_PADS(PAD_RGMII_TD1__RGMII_TD1	| MUX_PAD_CTRL(ENET_PAD_CTRL)),
	IOMUX_PADS(PAD_RGMII_TD2__RGMII_TD2	| MUX_PAD_CTRL(ENET_PAD_CTRL)),
	IOMUX_PADS(PAD_RGMII_TD3__RGMII_TD3	| MUX_PAD_CTRL(ENET_PAD_CTRL)),
	IOMUX_PADS(PAD_RGMII_TX_CTL__RGMII_TX_CTL	| MUX_PAD_CTRL(ENET_PAD_CTRL)),
	IOMUX_PADS(PAD_ENET_REF_CLK__ENET_TX_CLK	| MUX_PAD_CTRL(ENET_PAD_CTRL)),
	IOMUX_PADS(PAD_RGMII_RXC__RGMII_RXC	| MUX_PAD_CTRL(ENET_PAD_CTRL)),
	IOMUX_PADS(PAD_RGMII_RD0__RGMII_RD0	| MUX_PAD_CTRL(ENET_PAD_CTRL)),
	IOMUX_PADS(PAD_RGMII_RD1__RGMII_RD1	| MUX_PAD_CTRL(ENET_PAD_CTRL)),
	IOMUX_PADS(PAD_RGMII_RD2__RGMII_RD2	| MUX_PAD_CTRL(ENET_PAD_CTRL)),
	IOMUX_PADS(PAD_RGMII_RD3__RGMII_RD3	| MUX_PAD_CTRL(ENET_PAD_CTRL)),
	IOMUX_PADS(PAD_RGMII_RX_CTL__RGMII_RX_CTL	| MUX_PAD_CTRL(ENET_PAD_CTRL)),
	/* AR8031 PHY Reset */
	IOMUX_PADS(PAD_ENET_CRS_DV__GPIO1_IO25	| MUX_PAD_CTRL(NO_PAD_CTRL)),
};

static void setup_iomux_enet(void)
{
	SETUP_IOMUX_PADS(enet_pads);

	/* Reset AR8031 PHY */
	gpio_direction_output(IMX_GPIO_NR(1, 25) , 0);
	mdelay(10);
	gpio_set_value(IMX_GPIO_NR(1, 25), 1);
	udelay(100);
}

static iomux_v3_cfg_t const usdhc2_pads[] = {
	IOMUX_PADS(PAD_SD2_CLK__SD2_CLK	| MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD2_CMD__SD2_CMD	| MUX_PAD_CTRL(USDHC_PAD_CTRL)),
};

static iomux_v3_cfg_t const usdhc3_pads[] = {
	IOMUX_PADS(PAD_SD3_CLK__SD3_CLK   | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD3_CMD__SD3_CMD   | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD3_DAT0__SD3_DATA0 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD3_DAT1__SD3_DATA1 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD3_DAT2__SD3_DATA2 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD3_DAT3__SD3_DATA3 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD3_DAT4__SD3_DATA4 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD3_DAT5__SD3_DATA5 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD3_DAT6__SD3_DATA6 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD3_DAT7__SD3_DATA7 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_NANDF_D0__GPIO2_IO00    | MUX_PAD_CTRL(NO_PAD_CTRL)), /* CD */
};

static iomux_v3_cfg_t const usdhc4_pads[] = {
	IOMUX_PADS(PAD_SD4_CLK__SD4_CLK   | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD4_CMD__SD4_CMD   | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD4_DAT0__SD4_DATA0 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD4_DAT1__SD4_DATA1 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD4_DAT2__SD4_DATA2 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD4_DAT3__SD4_DATA3 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD4_DAT4__SD4_DATA4 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD4_DAT5__SD4_DATA5 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD4_DAT6__SD4_DATA6 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD4_DAT7__SD4_DATA7 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
};

#ifdef CONFIG_MXC_SPI
static iomux_v3_cfg_t const ecspi1_pads[] = {
	IOMUX_PADS(PAD_KEY_COL0__ECSPI1_SCLK | MUX_PAD_CTRL(SPI_PAD_CTRL)),
	IOMUX_PADS(PAD_KEY_COL1__ECSPI1_MISO | MUX_PAD_CTRL(SPI_PAD_CTRL)),
	IOMUX_PADS(PAD_KEY_ROW0__ECSPI1_MOSI | MUX_PAD_CTRL(SPI_PAD_CTRL)),
	IOMUX_PADS(PAD_KEY_ROW1__GPIO4_IO09 | MUX_PAD_CTRL(NO_PAD_CTRL)),
};

static void setup_spi(void)
{
	SETUP_IOMUX_PADS(ecspi1_pads);
}

int board_spi_cs_gpio(unsigned bus, unsigned cs)
{
	return (bus == 0 && cs == 0) ? (IMX_GPIO_NR(4, 9)) : -1;
}
#endif

static iomux_v3_cfg_t const backlight_pads[] = {
	/* Qseven MXM connector: LVDS_PPEN */
	IOMUX_PADS(PAD_ENET_TXD0__GPIO1_IO30 | MUX_PAD_CTRL(NO_PAD_CTRL)),
#define DISP0_PWR_EN	IMX_GPIO_NR(1, 30)

	/* Qseven MXM connector: LVDS_BLT_CTRL */
	IOMUX_PADS(PAD_SD1_DAT3__GPIO1_IO21 | MUX_PAD_CTRL(NO_PAD_CTRL)),
#define DISP0_PWM	IMX_GPIO_NR(1, 21)
};

static struct i2c_pads_info mx6q_i2c_pad_info1 = {
	.scl = {
		.i2c_mode = MX6Q_PAD_CSI0_DAT9__I2C1_SCL | I2C_PAD,
		.gpio_mode = MX6Q_PAD_CSI0_DAT9__GPIO5_IO27 | I2C_PAD,
		.gp = IMX_GPIO_NR(5, 27)
	},
	.sda = {
		.i2c_mode = MX6Q_PAD_CSI0_DAT8__I2C1_SDA | I2C_PAD,
		.gpio_mode = MX6Q_PAD_CSI0_DAT8__GPIO5_IO26 | I2C_PAD,
		.gp = IMX_GPIO_NR(5, 26)
	}
};

static struct i2c_pads_info mx6dl_i2c_pad_info1 = {
	.scl = {
		.i2c_mode = MX6DL_PAD_CSI0_DAT9__I2C1_SCL | I2C_PAD,
		.gpio_mode = MX6DL_PAD_CSI0_DAT9__GPIO5_IO27 | I2C_PAD,
		.gp = IMX_GPIO_NR(5, 27)
	},
	.sda = {
		.i2c_mode = MX6DL_PAD_CSI0_DAT8__I2C1_SDA | I2C_PAD,
		.gpio_mode = MX6DL_PAD_CSI0_DAT8__GPIO5_IO26 | I2C_PAD,
		.gp = IMX_GPIO_NR(5, 26)
	}
};

static struct i2c_pads_info mx6q_i2c_pad_info2 = {
	.scl = {
		.i2c_mode = MX6Q_PAD_KEY_COL3__I2C2_SCL | I2C_PAD,
		.gpio_mode = MX6Q_PAD_KEY_COL3__GPIO4_IO12 | I2C_PAD,
		.gp = IMX_GPIO_NR(4, 12)
	},
	.sda = {
		.i2c_mode = MX6Q_PAD_KEY_ROW3__I2C2_SDA | I2C_PAD,
		.gpio_mode = MX6Q_PAD_KEY_ROW3__GPIO4_IO13 | I2C_PAD,
		.gp = IMX_GPIO_NR(4, 13)
	}
};

static struct i2c_pads_info mx6dl_i2c_pad_info2 = {
	.scl = {
		.i2c_mode = MX6DL_PAD_KEY_COL3__I2C2_SCL | I2C_PAD,
		.gpio_mode = MX6DL_PAD_KEY_COL3__GPIO4_IO12 | I2C_PAD,
		.gp = IMX_GPIO_NR(4, 12)
	},
	.sda = {
		.i2c_mode = MX6DL_PAD_KEY_ROW3__I2C2_SDA | I2C_PAD,
		.gpio_mode = MX6DL_PAD_KEY_ROW3__GPIO4_IO13 | I2C_PAD,
		.gp = IMX_GPIO_NR(4, 13)
	}
};

static struct i2c_pads_info mx6q_i2c_pad_info3 = {
	.scl = {
		.i2c_mode = MX6Q_PAD_GPIO_3__I2C3_SCL | I2C_PAD,
		.gpio_mode = MX6Q_PAD_GPIO_3__GPIO1_IO03 | I2C_PAD,
		.gp = IMX_GPIO_NR(1, 3)
	},
	.sda = {
		.i2c_mode = MX6Q_PAD_GPIO_6__I2C3_SDA | I2C_PAD,
		.gpio_mode = MX6Q_PAD_GPIO_6__GPIO1_IO06 | I2C_PAD,
		.gp = IMX_GPIO_NR(1, 6)
	}
};

static struct i2c_pads_info mx6dl_i2c_pad_info3 = {
	.scl = {
		.i2c_mode = MX6DL_PAD_GPIO_3__I2C3_SCL | I2C_PAD,
		.gpio_mode = MX6DL_PAD_GPIO_3__GPIO1_IO03 | I2C_PAD,
		.gp = IMX_GPIO_NR(1, 3)
	},
	.sda = {
		.i2c_mode = MX6DL_PAD_GPIO_6__I2C3_SDA | I2C_PAD,
		.gpio_mode = MX6DL_PAD_GPIO_6__GPIO1_IO06 | I2C_PAD,
		.gp = IMX_GPIO_NR(1, 6)
	}
};

#ifdef CONFIG_PCIE_IMX
iomux_v3_cfg_t const pcie_pads[] = {
	IOMUX_PADS(PAD_EIM_D17__GPIO3_IO17 | MUX_PAD_CTRL(NO_PAD_CTRL)),	/* POWER */
	IOMUX_PADS(PAD_GPIO_17__GPIO7_IO12 | MUX_PAD_CTRL(NO_PAD_CTRL)),	/* RESET */
};

static void setup_pcie(void)
{
	SETUP_IOMUX_PADS(pcie_pads);
}
#endif

static void setup_iomux_i2c(void)
{
	if (is_mx6dq() || is_mx6dqp()) {
		setup_i2c(1, CONFIG_SYS_I2C_SPEED, 0x7f, &mx6q_i2c_pad_info1);
		setup_i2c(2, CONFIG_SYS_I2C_SPEED, 0x7f, &mx6q_i2c_pad_info2);
		setup_i2c(3, CONFIG_SYS_I2C_SPEED, 0x7f, &mx6q_i2c_pad_info3);
	}
	else {
		setup_i2c(1, CONFIG_SYS_I2C_SPEED, 0x7f, &mx6dl_i2c_pad_info1);
		setup_i2c(2, CONFIG_SYS_I2C_SPEED, 0x7f, &mx6dl_i2c_pad_info2);
		setup_i2c(3, CONFIG_SYS_I2C_SPEED, 0x7f, &mx6dl_i2c_pad_info3);
	}
}

static void setup_iomux_uart(void)
{
	SETUP_IOMUX_PADS(uart1_pads);
}

#ifdef CONFIG_FSL_ESDHC
struct fsl_esdhc_cfg usdhc_cfg[3] = {
	{USDHC2_BASE_ADDR},
	{USDHC3_BASE_ADDR},
	{USDHC4_BASE_ADDR},
};

#define USDHC3_CD_GPIO	IMX_GPIO_NR(2, 0)

int board_mmc_get_env_dev(int devno)
{
	return devno - 1;
}

int mmc_map_to_kernel_blk(int devno)
{
	return devno + 1;
}

int board_mmc_getcd(struct mmc *mmc)
{
	struct fsl_esdhc_cfg *cfg = (struct fsl_esdhc_cfg *)mmc->priv;
	int ret = 0;

	switch (cfg->esdhc_base) {
	case USDHC2_BASE_ADDR:
		ret = 0;
		break;
	case USDHC3_BASE_ADDR:
		ret = !gpio_get_value(USDHC3_CD_GPIO);
		break;
	case USDHC4_BASE_ADDR:
		ret = 1; /* eMMC/uSDHC4 is always present */
		break;
	}

	return ret;
}

int board_mmc_init(bd_t *bis)
{
#ifndef CONFIG_SPL_BUILD
	int ret;
	int i;

	/*
	 * According to the board_mmc_init() the following map is done:
	 * (U-Boot device node)    (Physical Port)
	 * mmc0                    SD2
	 * mmc1                    SD3
	 * mmc2                    eMMC
	 */
	for (i = 0; i < CONFIG_SYS_FSL_USDHC_NUM; i++) {
		switch (i) {
		case 0:
			SETUP_IOMUX_PADS(usdhc2_pads);
			usdhc_cfg[0].sdhc_clk = mxc_get_clock(MXC_ESDHC2_CLK);
			break;
		case 1:
			SETUP_IOMUX_PADS(usdhc3_pads);
			gpio_direction_input(USDHC3_CD_GPIO);
			usdhc_cfg[1].sdhc_clk = mxc_get_clock(MXC_ESDHC3_CLK);
			break;
		case 2:
			SETUP_IOMUX_PADS(usdhc4_pads);
			usdhc_cfg[2].sdhc_clk = mxc_get_clock(MXC_ESDHC4_CLK);
			break;
		default:
			printf("Warning: you configured more USDHC controllers"
			       "(%d) then supported by the board (%d)\n",
			       i + 1, CONFIG_SYS_FSL_USDHC_NUM);
			return -EINVAL;
		}

		ret = fsl_esdhc_initialize(bis, &usdhc_cfg[i]);
		if (ret)
			return ret;
	}

	return 0;
#else
	struct src *psrc = (struct src *)SRC_BASE_ADDR;
	unsigned reg = readl(&psrc->sbmr1) >> 11;
	/*
	 * Upon reading BOOT_CFG register the following map is done:
	 * Bit 11 and 12 of BOOT_CFG register can determine the current
	 * mmc port
	 * 0x1                  SD1
	 * 0x2                  SD2
	 * 0x3                  SD4
	 */

	switch (reg & 0x3) {
	case 0x1:
		SETUP_IOMUX_PADS(usdhc2_pads);
		usdhc_cfg[0].esdhc_base = USDHC2_BASE_ADDR;
		usdhc_cfg[0].sdhc_clk = mxc_get_clock(MXC_ESDHC2_CLK);
		gd->arch.sdhc_clk = usdhc_cfg[0].sdhc_clk;
		break;
	case 0x2:
		SETUP_IOMUX_PADS(usdhc3_pads);
		usdhc_cfg[0].esdhc_base = USDHC3_BASE_ADDR;
		usdhc_cfg[0].sdhc_clk = mxc_get_clock(MXC_ESDHC3_CLK);
		gd->arch.sdhc_clk = usdhc_cfg[0].sdhc_clk;
		break;
	case 0x3:
		SETUP_IOMUX_PADS(usdhc4_pads);
		usdhc_cfg[0].esdhc_base = USDHC4_BASE_ADDR;
		usdhc_cfg[0].sdhc_clk = mxc_get_clock(MXC_ESDHC4_CLK);
		gd->arch.sdhc_clk = usdhc_cfg[0].sdhc_clk;
		break;
	}

	return fsl_esdhc_initialize(bis, &usdhc_cfg[0]);
#endif /* CONFIG_SPL_BUILD */
}
#endif /* CONFIG_FSL_ESDHC */

static int handle_mac_address(char *env_var)
{
	unsigned char enetaddr[6];
	int rc;

	rc = eeprom_read_mac_addr(enetaddr);
	if (rc)
		return rc;
	if (!is_valid_ethaddr(enetaddr))
		return -1;

	return eth_env_set_enetaddr(env_var, enetaddr);
}

#define NO_MAC_ADDR             "No MAC address found from EEPROM for %s\n"
static int ar8031_phy_fixup(struct phy_device *phydev)
{
	unsigned short val;

	/* To enable AR8031 ouput a 125MHz clk from CLK_25M */
	phy_write(phydev, MDIO_DEVAD_NONE, 0xd, 0x7);
	phy_write(phydev, MDIO_DEVAD_NONE, 0xe, 0x8016);
	phy_write(phydev, MDIO_DEVAD_NONE, 0xd, 0x4007);

	val = phy_read(phydev, MDIO_DEVAD_NONE, 0xe);
	val &= 0xffe3;
	val |= 0x18;
	phy_write(phydev, MDIO_DEVAD_NONE, 0xe, val);

	/* introduce tx clock delay */
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1d, 0x5);
	val = phy_read(phydev, MDIO_DEVAD_NONE, 0x1e);
	val |= 0x0100;
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1e, val);

	return 0;
}

int board_phy_config(struct phy_device *phydev)
{
	ar8031_phy_fixup(phydev);

	if (phydev->drv->config)
		phydev->drv->config(phydev);

	if (handle_mac_address("ethaddr"))
		printf(NO_MAC_ADDR, "primary NIC");

	return 0;
}

/*
 * Do not overwrite the console
 * Use always serial for U-Boot console
 */
int overwrite_console(void)
{
	return 1;
}

int board_eth_init(bd_t *bis)
{
	setup_iomux_enet();

	return cpu_eth_init(bis);
}

#ifdef CONFIG_USB_EHCI_MX6
#define USB_OTHERREGS_OFFSET	0x800
#define UCTRL_PWR_POL		(1 << 9)

static iomux_v3_cfg_t const usb_otg_pads[] = {
	IOMUX_PADS(PAD_EIM_D22__USB_OTG_PWR | MUX_PAD_CTRL(NO_PAD_CTRL)),
	IOMUX_PADS(PAD_ENET_RX_ER__USB_OTG_ID | MUX_PAD_CTRL(NO_PAD_CTRL)),
};

static iomux_v3_cfg_t const usb_hc1_pads[] = {
	IOMUX_PADS(PAD_ENET_TXD1__GPIO1_IO29 | MUX_PAD_CTRL(NO_PAD_CTRL)),
};


int board_ehci_hcd_init(int port)
{
	u32 *usbnc_usb_ctrl;

	switch (port) {
	case 0:
		SETUP_IOMUX_PADS(usb_otg_pads);

		/*
	 	* set daisy chain for otg_pin_id on 6q.
	 	* for 6dl, this bit is reserved
	 	*/
		imx_iomux_set_gpr_register(1, 13, 1, 0);

		usbnc_usb_ctrl = (u32 *)(USB_BASE_ADDR + USB_OTHERREGS_OFFSET +
				 port * 4);

		setbits_le32(usbnc_usb_ctrl, UCTRL_PWR_POL);
		break;
	case 1:
		SETUP_IOMUX_PADS(usb_hc1_pads);
		break;
	default:
		printf("MXC USB port %d not yet supported\n", port);
		return -EINVAL;
	}

	return 0;
}

int board_ehci_power(int port, int on)
{
	switch (port) {
	case 0:
		break;
	case 1:
		if (on)
			gpio_direction_output(IMX_GPIO_NR(1, 29), 1);
		else
			gpio_direction_output(IMX_GPIO_NR(1, 29), 0);
		break;
	default:
		printf("MXC USB port %d not yet supported\n", port);
		return -EINVAL;
	}

	return 0;
}
#endif /* CONFIG_USB_EHCI_MX6 */

int board_early_init_f(void)
{
	setup_iomux_uart();

#ifdef CONFIG_MXC_SPI
	setup_spi();
#endif

#ifdef CONFIG_SATA
	setup_sata();
#endif

	return 0;
}

int board_init(void)
{
	/* address of boot parameters */
	gd->bd->bi_boot_params = PHYS_SDRAM + 0x100;

	setup_iomux_i2c();

#ifdef CONFIG_PCIE_IMX
	setup_pcie();
#endif
	return 0;
}

static struct pmic *pfuze_init(unsigned char i2cbus)
{
	struct pmic *p;
	int ret;
	u32 reg;

	ret = power_pfuze100_init(i2cbus);
	if (ret)
		return NULL;

	p = pmic_get("PFUZE100");
	ret = pmic_probe(p);
	if (ret)
		return NULL;

	pmic_reg_read(p, PFUZE100_DEVICEID, &reg);
	printf("PMIC:  PFUZE100 ID=0x%02x\n", reg);

	/* Set SW1AB stanby volage to 0.975V */
	pmic_reg_read(p, PFUZE100_SW1ABSTBY, &reg);
	reg &= ~SW1x_STBY_MASK;
	reg |= SW1x_0_975V;
	pmic_reg_write(p, PFUZE100_SW1ABSTBY, reg);

	/* Set SW1AB/VDDARM step ramp up time from 16us to 4us/25mV */
	pmic_reg_read(p, PFUZE100_SW1ABCONF, &reg);
	reg &= ~SW1xCONF_DVSSPEED_MASK;
	reg |= SW1xCONF_DVSSPEED_4US;
	pmic_reg_write(p, PFUZE100_SW1ABCONF, reg);

	/* Set SW1C standby voltage to 0.975V */
	pmic_reg_read(p, PFUZE100_SW1CSTBY, &reg);
	reg &= ~SW1x_STBY_MASK;
	reg |= SW1x_0_975V;
	pmic_reg_write(p, PFUZE100_SW1CSTBY, reg);

	/* Set SW1C/VDDSOC step ramp up time from 16us to 4us/25mV */
	pmic_reg_read(p, PFUZE100_SW1CCONF, &reg);
	reg &= ~SW1xCONF_DVSSPEED_MASK;
	reg |= SW1xCONF_DVSSPEED_4US;
	pmic_reg_write(p, PFUZE100_SW1CCONF, reg);

	return p;
}

static int pfuze_mode_init(struct pmic *p, u32 mode)
{
        unsigned char offset, i, switch_num;
        u32 id;
        int ret;

        pmic_reg_read(p, PFUZE100_DEVICEID, &id);
        id = id & 0xf;

        if (id == 0) {
                switch_num = 6;
                offset = PFUZE100_SW1CMODE;
        } else if (id == 1) {
                switch_num = 4;
                offset = PFUZE100_SW2MODE;
        } else {
                printf("Not supported, id=%d\n", id);
                return -EINVAL;
        }

        ret = pmic_reg_write(p, PFUZE100_SW1ABMODE, mode);
        if (ret < 0) {
                printf("Set SW1AB mode error!\n");
                return ret;
        }

        for (i = 0; i < switch_num - 1; i++) {
                ret = pmic_reg_write(p, offset + i * SWITCH_SIZE, mode);
                if (ret < 0) {
                        printf("Set switch 0x%x mode error!\n",
                               offset + i * SWITCH_SIZE);
                        return ret;
                }
        }

        return ret;
}

int power_init_board(void)
{
	struct pmic *p;
	unsigned int reg;
	int ret;

	p = pfuze_init(I2C_PMIC);
	if (!p)
		return -ENODEV;

	if (is_mx6dqp())
		ret = pfuze_mode_init(p, APS_APS);
	else
		ret = pfuze_mode_init(p, APS_PFM);

	if (ret < 0)
		return ret;

	/* VGEN3 and VGEN5 corrected on i.mx6qp board */
	if (!is_mx6dqp()) {
		/* Increase VGEN3 from 2.5 to 2.8V */
		pmic_reg_read(p, PFUZE100_VGEN3VOL, &reg);
		reg &= ~LDO_VOL_MASK;
		reg |= LDOB_2_80V;
		pmic_reg_write(p, PFUZE100_VGEN3VOL, reg);

		/* Increase VGEN5 from 2.8 to 3V */
		pmic_reg_read(p, PFUZE100_VGEN5VOL, &reg);
		reg &= ~LDO_VOL_MASK;
		reg |= LDOB_3_00V;
		pmic_reg_write(p, PFUZE100_VGEN5VOL, reg);
	}

	if (is_mx6dqp()) {
		/* set SW1C staby volatage 1.075V*/
		pmic_reg_read(p, PFUZE100_SW1CSTBY, &reg);
		reg &= ~0x3f;
		reg |= 0x1f;
		pmic_reg_write(p, PFUZE100_SW1CSTBY, reg);

		/* set SW1C/VDDSOC step ramp up time to from 16us to 4us/25mV */
		pmic_reg_read(p, PFUZE100_SW1CCONF, &reg);
		reg &= ~0xc0;
		reg |= 0x40;
		pmic_reg_write(p, PFUZE100_SW1CCONF, reg);

		/* set SW2/VDDARM staby volatage 0.975V*/
		pmic_reg_read(p, PFUZE100_SW2STBY, &reg);
		reg &= ~0x3f;
		reg |= 0x17;
		pmic_reg_write(p, PFUZE100_SW2STBY, reg);

		/* set SW2/VDDARM step ramp up time to from 16us to 4us/25mV */
		pmic_reg_read(p, PFUZE100_SW2CONF, &reg);
		reg &= ~0xc0;
		reg |= 0x40;
		pmic_reg_write(p, PFUZE100_SW2CONF, reg);
	} else {
		/* set SW1AB staby volatage 0.975V*/
		pmic_reg_read(p, PFUZE100_SW1ABSTBY, &reg);
		reg &= ~0x3f;
		reg |= 0x1b;
		pmic_reg_write(p, PFUZE100_SW1ABSTBY, reg);

		/* set SW1AB/VDDARM step ramp up time from 16us to 4us/25mV */
		pmic_reg_read(p, PFUZE100_SW1ABCONF, &reg);
		reg &= ~0xc0;
		reg |= 0x40;
		pmic_reg_write(p, PFUZE100_SW1ABCONF, reg);

		/* set SW1C staby volatage 0.975V*/
		pmic_reg_read(p, PFUZE100_SW1CSTBY, &reg);
		reg &= ~0x3f;
		reg |= 0x1b;
		pmic_reg_write(p, PFUZE100_SW1CSTBY, reg);

		/* set SW1C/VDDSOC step ramp up time to from 16us to 4us/25mV */
		pmic_reg_read(p, PFUZE100_SW1CCONF, &reg);
		reg &= ~0xc0;
		reg |= 0x40;
		pmic_reg_write(p, PFUZE100_SW1CCONF, reg);
	}

	return 0;
}

#ifdef CONFIG_LDO_BYPASS_CHECK
#ifdef CONFIG_POWER
void ldo_mode_set(int ldo_bypass)
{
        unsigned int value;
        int is_400M;
        unsigned char vddarm;
        struct pmic *p = pmic_get("PFUZE100");

        if (!p) {
                printf("No PMIC found!\n");
                return;
        }

        /* increase VDDARM/VDDSOC to support 1.2G chip */
        if (check_1_2G()) {
                ldo_bypass = 0; /* ldo_enable on 1.2G chip */
                printf("1.2G chip, increase VDDARM_IN/VDDSOC_IN\n");
                if (is_mx6dqp()) {
                        /* increase VDDARM to 1.425V */
                        pmic_reg_read(p, PFUZE100_SW2VOL, &value);
                        value &= ~0x3f;
                        value |= 0x29;
                        pmic_reg_write(p, PFUZE100_SW2VOL, value);
                } else {
                        /* increase VDDARM to 1.425V */
                        pmic_reg_read(p, PFUZE100_SW1ABVOL, &value);
                        value &= ~0x3f;
                        value |= 0x2d;
                        pmic_reg_write(p, PFUZE100_SW1ABVOL, value);
                }
                /* increase VDDSOC to 1.425V */
                pmic_reg_read(p, PFUZE100_SW1CVOL, &value);
                value &= ~0x3f;
                value |= 0x2d;
                pmic_reg_write(p, PFUZE100_SW1CVOL, value);
        }
        /* switch to ldo_bypass mode , boot on 800Mhz */
        if (ldo_bypass) {
                prep_anatop_bypass();
                if (is_mx6dqp()) {
                        /* decrease VDDARM for 400Mhz DQP:1.1V*/
                        pmic_reg_read(p, PFUZE100_SW2VOL, &value);
                        value &= ~0x3f;
                        value |= 0x1c;
                        pmic_reg_write(p, PFUZE100_SW2VOL, value);
                } else {
                        /* decrease VDDARM for 400Mhz DQ:1.1V, DL:1.275V */
                        pmic_reg_read(p, PFUZE100_SW1ABVOL, &value);
                        value &= ~0x3f;
                        if (is_mx6dl())
                                value |= 0x27;
                        else
                                value |= 0x20;

                        pmic_reg_write(p, PFUZE100_SW1ABVOL, value);
                }
                /* increase VDDSOC to 1.3V */
                pmic_reg_read(p, PFUZE100_SW1CVOL, &value);
                value &= ~0x3f;
                value |= 0x28;
                pmic_reg_write(p, PFUZE100_SW1CVOL, value);

                /*
                 * MX6Q/DQP:
                 * VDDARM:1.15V@800M; VDDSOC:1.175V@800M
                 * VDDARM:0.975V@400M; VDDSOC:1.175V@400M
                 * MX6DL:
                 * VDDARM:1.175V@800M; VDDSOC:1.175V@800M
                 * VDDARM:1.15V@400M; VDDSOC:1.175V@400M
                 */
                is_400M = set_anatop_bypass(2);
                if (is_mx6dqp()) {
                        pmic_reg_read(p, PFUZE100_SW2VOL, &value);
                        value &= ~0x3f;
                        if (is_400M)
                                value |= 0x17;
                        else
                                value |= 0x1e;
                        pmic_reg_write(p, PFUZE100_SW2VOL, value);
                }

                if (is_400M) {
                        if (is_mx6dl())
                                vddarm = 0x22;
                        else
                                vddarm = 0x1b;
                } else {
                        if (is_mx6dl())
                                vddarm = 0x23;
                        else
                                vddarm = 0x22;
                }
                pmic_reg_read(p, PFUZE100_SW1ABVOL, &value);
                value &= ~0x3f;
                value |= vddarm;
                pmic_reg_write(p, PFUZE100_SW1ABVOL, value);

                /* decrease VDDSOC to 1.175V */
                pmic_reg_read(p, PFUZE100_SW1CVOL, &value);
                value &= ~0x3f;
                value |= 0x23;
                pmic_reg_write(p, PFUZE100_SW1CVOL, value);

                finish_anatop_bypass();
                printf("switch to ldo_bypass mode!\n");
        }
}
#endif
#endif

#ifdef CONFIG_CMD_BMODE
static const struct boot_mode board_boot_modes[] = {
	/* 4 bit bus width */
	{"sd3",	 MAKE_CFGVAL(0x40, 0x30, 0x00, 0x00)},
	/* 8 bit bus width */
	{"emmc", MAKE_CFGVAL(0x60, 0x58, 0x00, 0x00)},
	{NULL,	 0},
};
#endif

int board_late_init(void)
{
#ifdef CONFIG_CMD_BMODE
	add_board_boot_modes(board_boot_modes);
#endif

	env_set("tee", "no");
#ifdef CONFIG_IMX_OPTEE
	env_set("tee", "yes");
#endif

#ifdef CONFIG_ENV_VARS_UBOOT_RUNTIME_CONFIG
	env_set("board_name", "q7m120");

	if (is_mx6dqp())
		env_set("board_rev", "MX6QP");
	else if (is_mx6dq())
		env_set("board_rev", "MX6Q");
	else if (is_mx6sdl())
		env_set("board_rev", "MX6DL");
#endif

#ifdef CONFIG_ENV_IS_IN_MMC
	board_late_mmc_env_init();
#endif

	return 0;
}

int checkboard(void)
{
	puts("Model: Axiomtek Q7M120\n");
	return 0;
}

#ifdef CONFIG_SPL_BUILD
#include <asm/arch/mx6-ddr.h>
#include <spl.h>
#include <linux/libfdt.h>

#ifdef CONFIG_SPL_OS_BOOT
int spl_start_uboot(void)
{
	gpio_direction_input(KEY_VOL_UP);

	/* Only enter in Falcon mode if KEY_VOL_UP is pressed */
	return gpio_get_value(KEY_VOL_UP);
}
#endif

/* Configure MX6Q/DUAL mmdc DDR io registers */
static const struct mx6dq_iomux_ddr_regs mx6q_ddr_ioregs = {
	/* SDCLK[0:1], CAS, RAS, Reset: */
	.dram_sdclk_0           = 0x00000030,
	.dram_sdclk_1           = 0x00000030,
	.dram_cas               = 0x00000030,
	.dram_ras               = 0x00000030,
	.dram_reset             = 0x00000030,
	/* SDCKE[0:1]: */
	.dram_sdcke0            = 0x00000000,
	.dram_sdcke1            = 0x00000000,
	/* SDBA2: */
	.dram_sdba2             = 0x00000000,
	/* SDODT[0:1]: */
	.dram_sdodt0            = 0x00000030,
	.dram_sdodt1            = 0x00000030,
	/* SDQS[0:7]: */
	.dram_sdqs0             = 0x00000028,
	.dram_sdqs1             = 0x00000028,
	.dram_sdqs2             = 0x00000028,
	.dram_sdqs3             = 0x00000028,
	.dram_sdqs4             = 0x00000028,
	.dram_sdqs5             = 0x00000028,
	.dram_sdqs6             = 0x00000028,
	.dram_sdqs7             = 0x00000028,
	/* DQM[0:7]: */
	.dram_dqm0              = 0x00000028,
	.dram_dqm1              = 0x00000028,
	.dram_dqm2              = 0x00000028,
	.dram_dqm3              = 0x00000028,
	.dram_dqm4              = 0x00000028,
	.dram_dqm5              = 0x00000028,
	.dram_dqm6              = 0x00000028,
	.dram_dqm7              = 0x00000028,
};

/* Configure MX6DL/Solo mmdc DDR io registers */
static const struct mx6sdl_iomux_ddr_regs mx6dl_ddr_ioregs = {
	/* SDCLK[0:1], CAS, RAS, Reset: */
	.dram_sdclk_0           = 0x00000030,
	.dram_sdclk_1           = 0x00000030,
	.dram_cas               = 0x00000030,
	.dram_ras               = 0x00000030,
	.dram_reset             = 0x00000030,
	/* SDCKE[0:1]: */
	.dram_sdcke0            = 0x00000000,
	.dram_sdcke1            = 0x00000000,
	/* SDBA2: */
	.dram_sdba2             = 0x00000000,
	/* SDODT[0:1]: */
	.dram_sdodt0            = 0x00000030,
	.dram_sdodt1            = 0x00000030,
	/* SDQS[0:7]: */
	.dram_sdqs0             = 0x00000030,
	.dram_sdqs1             = 0x00000030,
	.dram_sdqs2             = 0x00000030,
	.dram_sdqs3             = 0x00000030,
	.dram_sdqs4             = 0x00000030,
	.dram_sdqs5             = 0x00000030,
	.dram_sdqs6             = 0x00000030,
	.dram_sdqs7             = 0x00000030,
	/* DQM[0:7]: */
	.dram_dqm0              = 0x00000030,
	.dram_dqm1              = 0x00000030,
	.dram_dqm2              = 0x00000030,
	.dram_dqm3              = 0x00000030,
	.dram_dqm4              = 0x00000030,
	.dram_dqm5              = 0x00000030,
	.dram_dqm6              = 0x00000030,
	.dram_dqm7              = 0x00000030,
};

/* Configure MX6Q/DUAL mmdc GRP io registers */
static const struct mx6dq_iomux_grp_regs mx6q_grp_ioregs = {
	/* DDR3 */
	.grp_ddr_type           = 0x000c0000,
	.grp_ddrmode_ctl        = 0x00020000,
	/* Disable DDR pullups */
	.grp_ddrpke             = 0x00000000,
	/* ADDR[00:16], SDBA[0:1]: */
	.grp_addds              = 0x00000030,
	/* CS0/CS1/SDBA2/CKE0/CKE1/SDWE: */
	.grp_ctlds              = 0x00000030,
	/* DATA[00:63]: */
	.grp_ddrmode            = 0x00020000,
	.grp_b0ds               = 0x00000028,
	.grp_b1ds               = 0x00000028,
	.grp_b2ds               = 0x00000028,
	.grp_b3ds               = 0x00000028,
	.grp_b4ds               = 0x00000028,
	.grp_b5ds               = 0x00000028,
	.grp_b6ds               = 0x00000028,
	.grp_b7ds               = 0x00000028,
};

/* Configure MX6DL/Solo mmdc GRP io registers */
static const struct mx6sdl_iomux_grp_regs mx6dl_grp_ioregs = {
	/* DDR3 */
	.grp_ddr_type           = 0x000c0000,
	.grp_ddrmode_ctl        = 0x00020000,
	/* Disable DDR pullups */
	.grp_ddrpke             = 0x00000000,
	/* ADDR[00:16], SDBA[0:1]: */
	.grp_addds              = 0x00000030,
	/* CS0/CS1/SDBA2/CKE0/CKE1/SDWE: */
	.grp_ctlds              = 0x00000030,
	/* DATA[00:63]: */
	.grp_ddrmode            = 0x00020000,
	.grp_b0ds               = 0x00000030,
	.grp_b1ds               = 0x00000030,
	.grp_b2ds               = 0x00000030,
	.grp_b3ds               = 0x00000030,
	.grp_b4ds               = 0x00000030,
	.grp_b5ds               = 0x00000030,
	.grp_b6ds               = 0x00000030,
	.grp_b7ds               = 0x00000030,
};

static struct mx6_mmdc_calibration mx6_mmdc_calib = {
	/* write leveling calibration determine */
	.p0_mpwldectrl0 	= 0x00190017,
	.p0_mpwldectrl1 	= 0x001E0017,
	.p1_mpwldectrl0 	= 0x00150021,
	.p1_mpwldectrl1 	= 0x0014001F,
	/* read DQS gating calibration */
	.p0_mpdgctrl0 		= 0x42380244,
	.p0_mpdgctrl1 		= 0x0234022C,
	.p1_mpdgctrl0 		= 0x42380244,
	.p1_mpdgctrl1 		= 0x02380214,
	/* read calibration: DQS delay relative to DQ read access */
	.p0_mprddlctl 		= 0x44383C40,
	.p1_mprddlctl 		= 0x40383844,
	/* write calibration: DQ/DM delay relative to DQS write access */
	.p0_mpwrdlctl 		= 0x343C3E38,
	.p1_mpwrdlctl 		= 0x40344236,
};

static struct mx6_ddr_sysinfo q7m120_sysinfo_q = {
	.ddr_type 	= DDR_TYPE_DDR3,
	/* width of data bus:0=16,1=32,2=64 */
	.dsize 		= 2,
	/* Config for full 4GB range so that get_mem_size() works */
	.cs_density     = 32,	/* 32Gb per CS */
	/* single chip select */
	.ncs 		= 1,
	.cs1_mirror 	= 0,
	.rtt_wr         = 0,    /* RTT_Wr = RZQ/4 */
	.rtt_nom        = 2,    /* RTT_Nom = RZQ/2 */
	.walat          = 0,    /* Write additional latency */
	.ralat          = 5,    /* Read additional latency */
	.mif3_mode      = 3,    /* Command prediction working mode */
	.bi_on          = 1,    /* Bank interleaving enabled */
	.sde_to_rst     = 0x10, /* 14 cycles, 200us (JEDEC default) */
	.rst_to_cke     = 0x23, /* 33 cycles, 500us (JEDEC default) */
	.refsel 	= 1,    /* Refresh cycles at 32KHz */
	.refr 		= 3,      /* 8 refresh commands per refresh cycle */
};

/* Micron MT41K128M16JT-125*/
static struct mx6_ddr3_cfg mt41k128m16jt_125 = {
        .mem_speed = 1600,
        .density = 2,
        .width = 64,
        .banks = 8,
        .rowaddr = 14,
        .coladdr = 10,
        .pagesz = 2,
        .trcd = 1375,
        .trcmin = 4875,
        .trasmin = 3500,
};

/* Micron MT41K256M16TW-107 */
static struct mx6_ddr3_cfg mt41k256m16tw_107 = {
        .mem_speed = 1600,
        .density = 4,
        .width = 64,
        .banks = 8,
        .rowaddr = 15,
        .coladdr = 10,
        .pagesz = 2,
        .trcd = 1375,
        .trcmin = 4875,
        .trasmin = 3500,
};

static void ccgr_init(void)
{
	struct mxc_ccm_reg *ccm = (struct mxc_ccm_reg *)CCM_BASE_ADDR;

	writel(0x00C03F3F, &ccm->CCGR0);
	writel(0x0030FC03, &ccm->CCGR1);
	writel(0x0FFFC000, &ccm->CCGR2);
	writel(0x3FF00000, &ccm->CCGR3);
	writel(0x00FFF300, &ccm->CCGR4);
	writel(0x0F0000C3, &ccm->CCGR5);
	writel(0x000003FF, &ccm->CCGR6);
}

static int mx6qp_dcd_table[] = {
	0x020e0798, 0x000c0000,
	0x020e0758, 0x00000000,
	0x020e0588, 0x00000030,
	0x020e0594, 0x00000030,
	0x020e056c, 0x00000030,
	0x020e0578, 0x00000030,
	0x020e074c, 0x00000030,
	0x020e057c, 0x00000030,
	0x020e058c, 0x00000000,
	0x020e059c, 0x00000030,
	0x020e05a0, 0x00000030,
	0x020e078c, 0x00000030,
	0x020e0750, 0x00020000,
	0x020e05a8, 0x00000030,
	0x020e05b0, 0x00000030,
	0x020e0524, 0x00000030,
	0x020e051c, 0x00000030,
	0x020e0518, 0x00000030,
	0x020e050c, 0x00000030,
	0x020e05b8, 0x00000030,
	0x020e05c0, 0x00000030,
	0x020e0774, 0x00020000,
	0x020e0784, 0x00000030,
	0x020e0788, 0x00000030,
	0x020e0794, 0x00000030,
	0x020e079c, 0x00000030,
	0x020e07a0, 0x00000030,
	0x020e07a4, 0x00000030,
	0x020e07a8, 0x00000030,
	0x020e0748, 0x00000030,
	0x020e05ac, 0x00000030,
	0x020e05b4, 0x00000030,
	0x020e0528, 0x00000030,
	0x020e0520, 0x00000030,
	0x020e0514, 0x00000030,
	0x020e0510, 0x00000030,
	0x020e05bc, 0x00000030,
	0x020e05c4, 0x00000030,
	0x021b0800, 0xa1390003,
	0x021b080c, 0x001b001e,
	0x021b0810, 0x002e0029,
	0x021b480c, 0x001b002a,
	0x021b4810, 0x0019002c,
	0x021b083c, 0x43240334,
	0x021b0840, 0x0324031a,
	0x021b483c, 0x43340344,
	0x021b4840, 0x03280276,
	0x021b0848, 0x44383A3E,
	0x021b4848, 0x3C3C3846,
	0x021b0850, 0x2e303230,
	0x021b4850, 0x38283E34,
	0x021b081c, 0x33333333,
	0x021b0820, 0x33333333,
	0x021b0824, 0x33333333,
	0x021b0828, 0x33333333,
	0x021b481c, 0x33333333,
	0x021b4820, 0x33333333,
	0x021b4824, 0x33333333,
	0x021b4828, 0x33333333,
	0x021b08c0, 0x24912249,
	0x021b48c0, 0x24914289,
	0x021b08b8, 0x00000800,
	0x021b48b8, 0x00000800,
	0x021b0004, 0x00020036,
	0x021b0008, 0x24444040,
	0x021b000c, 0x555A7955,
	0x021b0010, 0xFF320F64,
	0x021b0014, 0x01ff00db,
	0x021b0018, 0x00001740,
	0x021b001c, 0x00008000,
	0x021b002c, 0x000026d2,
	0x021b0030, 0x005A1023,
	0x021b0040, 0x00000027,
	0x021b0400, 0x14420000,
	0x021b0000, 0x831A0000,
	0x021b0890, 0x00400C58,
	0x00bb0008, 0x00000000,
	0x00bb000c, 0x2891E41A,
	0x00bb0038, 0x00000564,
	0x00bb0014, 0x00000040,
	0x00bb0028, 0x00000020,
	0x00bb002c, 0x00000020,
	0x021b001c, 0x04088032,
	0x021b001c, 0x00008033,
	0x021b001c, 0x00048031,
	0x021b001c, 0x09408030,
	0x021b001c, 0x04008040,
	0x021b0020, 0x00005800,
	0x021b0818, 0x00011117,
	0x021b4818, 0x00011117,
	0x021b0004, 0x00025576,
	0x021b0404, 0x00011006,
	0x021b001c, 0x00000000,
};

static void ddr_init(int *table, int size)
{
	int i;

	for (i = 0; i < size / 2 ; i++)
		writel(table[2 * i + 1], table[2 * i]);
}

static void spl_dram_init(void)
{
	if (is_mx6dq()) {
		mx6dq_dram_iocfg(64, &mx6q_ddr_ioregs, &mx6q_grp_ioregs);
		if (CONFIG_DDR_MB == 2048)
			mx6_dram_cfg(&q7m120_sysinfo_q, &mx6_mmdc_calib, &mt41k256m16tw_107);
		else
			mx6_dram_cfg(&q7m120_sysinfo_q, &mx6_mmdc_calib, &mt41k128m16jt_125);
	}
	else if (is_mx6dqp()) {
		ddr_init(mx6qp_dcd_table, ARRAY_SIZE(mx6qp_dcd_table));
	}
	else if (is_mx6sdl()) {
		mx6sdl_dram_iocfg(64, &mx6dl_ddr_ioregs, &mx6dl_grp_ioregs);
		if (CONFIG_DDR_MB == 2048)
			mx6_dram_cfg(&q7m120_sysinfo_q, &mx6_mmdc_calib, &mt41k256m16tw_107);
		else
			mx6_dram_cfg(&q7m120_sysinfo_q, &mx6_mmdc_calib, &mt41k128m16jt_125);
	}
}

void board_init_f(ulong dummy)
{
	/* setup AIPS and disable watchdog */
	arch_cpu_init();

	ccgr_init();
	gpr_init();

	/* iomux and setup of i2c */
	board_early_init_f();

	/* setup GP timer */
	timer_init();

	/* UART clocks enabled and gd valid - init serial console */
	preloader_console_init();

	/* Needed for malloc() to work in SPL prior to board_init_r() */
	spl_init();

	/* DDR initialization */
	spl_dram_init();

	/* DDR calibration */
	udelay(100);
	mmdc_do_write_level_calibration(&q7m120_sysinfo_q);
	mmdc_do_dqs_calibration(&q7m120_sysinfo_q);

	/* Clear the BSS. */
	memset(__bss_start, 0, __bss_end - __bss_start);

	/* load/boot image from boot device */
	board_init_r(NULL, 0);
}
#endif

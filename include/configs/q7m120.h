/*
 * Copyright (C) 2019 Axiomtek Co., Ltd.
 *
 * Configuration settings for the Axiomtek i.MX6 Q7M120 SoM.
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#ifndef __Q7M120_CONFIG_H
#define __Q7M120_CONFIG_H

#include "mx6_common.h"

#ifdef CONFIG_SPL
#include "imx6_spl.h"
#endif

#define CONFIG_IMX_THERMAL

/* Size of malloc() pool */
#define CONFIG_SYS_MALLOC_LEN           (10 * SZ_1M)

#define CONFIG_MXC_UART

#define CONFIG_MACH_TYPE	4933
#define CONFIG_MXC_UART_BASE	UART1_BASE
#define CONSOLE_DEV		"ttymxc0"

#define CONFIG_SUPPORT_EMMC_BOOT /* eMMC specific */

/* Falcon Mode */
#define CONFIG_SPL_FS_LOAD_ARGS_NAME	"args"
#define CONFIG_SPL_FS_LOAD_KERNEL_NAME	"uImage"
#define CONFIG_SYS_SPL_ARGS_ADDR       0x18000000
#define CONFIG_CMD_SPL_WRITE_SIZE      (128 * SZ_1K)

/* Falcon Mode - MMC support: args@1MB kernel@2MB */
#define CONFIG_SYS_MMCSD_RAW_MODE_ARGS_SECTOR  0x800   /* 1MB */
#define CONFIG_SYS_MMCSD_RAW_MODE_ARGS_SECTORS (CONFIG_CMD_SPL_WRITE_SIZE / 512)
#define CONFIG_SYS_MMCSD_RAW_MODE_KERNEL_SECTOR        0x1000  /* 2MB */

/* MMC Configs */
#define CONFIG_SYS_FSL_ESDHC_ADDR      0

#define CONFIG_FEC_MXC
#define CONFIG_MII
#define IMX_FEC_BASE                    ENET_BASE_ADDR
#define CONFIG_FEC_XCV_TYPE             RGMII
#define CONFIG_ETHPRIME                 "FEC"
#define CONFIG_FEC_MXC_PHYADDR          1

#define CONFIG_PHYLIB
#define CONFIG_PHY_ATHEROS

#ifdef CONFIG_CMD_SF
#define CONFIG_MXC_SPI
#define CONFIG_SF_DEFAULT_BUS           0
#define CONFIG_SF_DEFAULT_CS            0
#define CONFIG_SF_DEFAULT_SPEED         20000000
#define CONFIG_SF_DEFAULT_MODE          SPI_MODE_0
#endif

#define CONFIG_ENV_VARS_UBOOT_RUNTIME_CONFIG

#if defined(CONFIG_SATA_BOOT)
#define CONFIG_EXTRA_ENV_SETTINGS \
	TEE_ENV \
	"script=boot.scr\0" \
        "image=zImage\0" \
        "fdt_file=undefined\0" \
        "fdt_addr=0x18000000\0" \
        "fdt_high=0xffffffff\0"   \
        "tee_addr=0x20000000\0" \
        "tee_file=undefined\0" \
	"findfdt="\
		"if test $fdt_file = undefined; then " \
			"if test $board_name = q7m120 && test $board_rev = MX6QP; then " \
				"setenv fdt_file q7m120qp-evk.dtb; fi; " \
                        "if test $board_name = q7m120 && test $board_rev = MX6Q; then " \
                                "setenv fdt_file q7m120q-evk.dtb; fi; " \
                        "if test $board_name = q7m120 && test $board_rev = MX6DL; then " \
                                "setenv fdt_file q7m120dl-evk.dtb; fi; " \
                        "if test $fdt_file = undefined; then " \
                                "echo WARNING: Could not determine dtb to use; fi; " \
                "fi;\0" \
        "findtee="\
                "if test $tee_file = undefined; then " \
                        "if test $board_name = q7m120 && test $board_rev = MX6QP; then " \
                                "setenv tee_file uTee-q7m120qp; fi; " \
                        "if test $board_name = q7m120 && test $board_rev = MX6Q; then " \
                                "setenv tee_file uTee-q7m120q; fi; " \
                        "if test $board_name = q7m120 && test $board_rev = MX6DL; then " \
                                "setenv tee_file uTee-q7m120dl; fi; " \
                        "if test $fdt_file = undefined; then " \
                                "echo WARNING: Could not determine dtb to use; fi; " \
                "fi;\0" \
	"bootargs=console=" CONSOLE_DEV ",115200 \0" \
	"bootargs_sata=setenv bootargs ${bootargs} " \
		"root=/dev/sda2 rootwait rw \0" \
	"loadbootscript=" \
		"fatload sata 0:1 ${loadaddr} ${script};\0" \
	"bootscript=echo Running bootscript from sata ...; " \
		"source\0" \
	"bootcmd_sata=run bootargs_sata; sata init; " \
		"run findfdt; run findtee;" \
		"if run loadbootscript; then " \
		"run bootscript; " \
		"else " \
		"fatload sata 0:1 ${loadaddr} ${image}; " \
		"fatload sata 0:1 ${fdt_addr} ${fdt_file}; " \
		"if test ${tee} = yes; then " \
			"fatload sata 0:1 ${tee_addr} ${tee_file}; " \
			"bootm ${tee_addr} - ${fdt_addr}; " \
		"else " \
			"bootz ${loadaddr} - ${fdt_addr}; " \
		"fi; " \
		"fi \0"\
	"bootcmd=run bootcmd_sata \0"
#else
#define CONFIG_EXTRA_ENV_SETTINGS \
	TEE_ENV \
        "script=boot.scr\0" \
        "image=zImage\0" \
        "fdt_file=undefined\0" \
        "fdt_addr=0x18000000\0" \
        "tee_addr=0x20000000\0" \
        "tee_file=undefined\0" \
        "boot_fdt=try\0" \
        "ip_dyn=yes\0" \
        "console=" CONSOLE_DEV "\0" \
        "dfuspi=dfu 0 sf 0:0:10000000:0\0" \
        "dfu_alt_info_spl=spl raw 0x400\0" \
        "dfu_alt_info_img=u-boot raw 0x10000\0" \
        "dfu_alt_info=spl raw 0x400\0" \
        "fdt_high=0xffffffff\0"   \
        "initrd_high=0xffffffff\0" \
        "mmcdev=" __stringify(CONFIG_SYS_MMC_ENV_DEV) "\0" \
        "mmcpart=1\0" \
        "finduuid=part uuid mmc ${mmcdev}:2 uuid\0" \
        "mmcargs=setenv bootargs console=${console},${baudrate} " \
                "root=PARTUUID=${uuid} rootwait rw\0" \
        "loadbootscript=" \
                "fatload mmc ${mmcdev}:${mmcpart} ${loadaddr} ${script};\0" \
        "bootscript=echo Running bootscript from mmc ...; " \
                "source\0" \
        "loadimage=fatload mmc ${mmcdev}:${mmcpart} ${loadaddr} ${image}\0" \
        "loadfdt=fatload mmc ${mmcdev}:${mmcpart} ${fdt_addr} ${fdt_file}\0" \
        "loadtee=fatload mmc ${mmcdev}:${mmcpart} ${tee_addr} ${tee_file}\0" \
        "mmcboot=echo Booting from mmc ...; " \
                "run finduuid; " \
                "run mmcargs; " \
		"if test ${tee} = yes; then " \
			"run loadfdt; run loadtee; bootm ${tee_addr} - ${fdt_addr}; " \
		"else " \
                	"if test ${boot_fdt} = yes || test ${boot_fdt} = try; then " \
                        	"if run loadfdt; then " \
                                	"bootz ${loadaddr} - ${fdt_addr}; " \
                        	"else " \
                                	"if test ${boot_fdt} = try; then " \
                                        	"bootz; " \
                                	"else " \
                                        	"echo WARN: Cannot load the DT; " \
                                	"fi; " \
                        	"fi; " \
                	"else " \
                        	"bootz; " \
                        "fi; " \
                "fi;\0" \
        "netargs=setenv bootargs console=${console},${baudrate} " \
                "root=/dev/nfs " \
                "ip=dhcp nfsroot=${serverip}:${nfsroot},v3,tcp\0" \
        "netboot=echo Booting from net ...; " \
                "run netargs; " \
                "if test ${ip_dyn} = yes; then " \
                        "setenv get_cmd dhcp; " \
                "else " \
                        "setenv get_cmd tftp; " \
                "fi; " \
                "${get_cmd} ${image}; " \
		"if test ${tee} = yes; then " \
			"${get_cmd} ${tee_addr} ${tee_file}; " \
			"${get_cmd} ${fdt_addr} ${fdt_file}; " \
			"bootm ${tee_addr} - ${fdt_addr}; " \
		"else " \
                	"if test ${boot_fdt} = yes || test ${boot_fdt} = try; then " \
                        	"if ${get_cmd} ${fdt_addr} ${fdt_file}; then " \
                                	"bootz ${loadaddr} - ${fdt_addr}; " \
                        	"else " \
                                	"if test ${boot_fdt} = try; then " \
                                        	"bootz; " \
                                	"else " \
                                        	"echo WARN: Cannot load the DT; " \
                                	"fi; " \
                        	"fi; " \
                	"else " \
                        	"bootz; " \
                        "fi; " \
                "fi;\0" \
                "findfdt="\
                        "if test $fdt_file = undefined; then " \
                                "if test $board_name = q7m120 && test $board_rev = MX6QP; then " \
                                        "setenv fdt_file q7m120qp-evk.dtb; fi; " \
                                "if test $board_name = q7m120 && test $board_rev = MX6Q; then " \
                                        "setenv fdt_file q7m120q-evk.dtb; fi; " \
                                "if test $board_name = q7m120 && test $board_rev = MX6DL; then " \
                                        "setenv fdt_file q7m120dl-evk.dtb; fi; " \
                                "if test $fdt_file = undefined; then " \
                                        "echo WARNING: Could not determine dtb to use; fi; " \
                        "fi;\0" \
                "findtee="\
                        "if test $tee_file = undefined; then " \
                                "if test $board_name = q7m120 && test $board_rev = MX6QP; then " \
                                        "setenv tee_file uTee-q7m120qp; fi; " \
                                "if test $board_name = q7m120 && test $board_rev = MX6Q; then " \
                                        "setenv tee_file uTee-q7m120q; fi; " \
                                "if test $board_name = q7m120 && test $board_rev = MX6DL; then " \
                                        "setenv tee_file uTee-q7m120dl; fi; " \
                                "if test $fdt_file = undefined; then " \
                                        "echo WARNING: Could not determine dtb to use; fi; " \
                        "fi;\0" \

#define CONFIG_BOOTCOMMAND \
        "run findfdt;" \
        "run findtee;" \
        "mmc dev ${mmcdev};" \
        "if mmc rescan; then " \
                "if run loadbootscript; then " \
                "run bootscript; " \
                "else " \
                        "if run loadimage; then " \
                                "run mmcboot; " \
                        "else run netboot; " \
                        "fi; " \
                "fi; " \
        "else run netboot; fi"
#endif /* CONFIG_SATA_BOOT */

#define CONFIG_ARP_TIMEOUT     200UL

#define CONFIG_SYS_MEMTEST_START       0x10000000
#define CONFIG_SYS_MEMTEST_END         0x10010000
#define CONFIG_SYS_MEMTEST_SCRATCH     0x10800000

/* Physical Memory Map */
#define CONFIG_NR_DRAM_BANKS           1
#define PHYS_SDRAM                     MMDC0_ARB_BASE_ADDR

#define CONFIG_SYS_SDRAM_BASE          PHYS_SDRAM
#define CONFIG_SYS_INIT_RAM_ADDR       IRAM_BASE_ADDR
#define CONFIG_SYS_INIT_RAM_SIZE       IRAM_SIZE

#define CONFIG_SYS_INIT_SP_OFFSET \
        (CONFIG_SYS_INIT_RAM_SIZE - GENERATED_GBL_DATA_SIZE)
#define CONFIG_SYS_INIT_SP_ADDR \
        (CONFIG_SYS_INIT_RAM_ADDR + CONFIG_SYS_INIT_SP_OFFSET)

/* Environment organization */
#define CONFIG_ENV_SIZE                 (8 * 1024)
#define CONFIG_ENV_OFFSET               (768 * 1024)

#ifdef CONFIG_SATA
#define CONFIG_DWC_AHSATA
#define CONFIG_SYS_SATA_MAX_DEVICE	1
#define CONFIG_DWC_AHSATA_PORT_ID	0
#define CONFIG_DWC_AHSATA_BASE_ADDR	SATA_ARB_BASE_ADDR
#define CONFIG_LBA48
#define CONFIG_LIBATA
#endif

#define CONFIG_SYS_FSL_USDHC_NUM	3
#if defined(CONFIG_ENV_IS_IN_MMC)
#define CONFIG_SYS_MMC_ENV_DEV		1	/* SDHC3 */
#elif defined(CONFIG_ENV_IS_IN_SATA)
#define CONFIG_SYS_SATA_ENV_DEV		0
#define CONFIG_SYS_DCACHE_OFF /* remove when sata driver support cache */
#endif

/* I2C Configs */
#define CONFIG_SYS_I2C
#define CONFIG_SYS_I2C_MXC
#define CONFIG_SYS_I2C_MXC_I2C1		/* enable I2C bus 1 */
#define CONFIG_SYS_I2C_MXC_I2C2		/* enable I2C bus 2 */
#define CONFIG_SYS_I2C_MXC_I2C3		/* enable I2C bus 3 */
#define CONFIG_SYS_I2C_SPEED		  100000

/* EEPROM configs */
#define CONFIG_SYS_I2C_EEPROM_BUS       1
#define CONFIG_SYS_I2C_EEPROM_ADDR      0x51

/* PMIC */
#define CONFIG_POWER
#define CONFIG_POWER_I2C
#define CONFIG_POWER_PFUZE100
#define CONFIG_POWER_PFUZE100_I2C_ADDR	0x08

/* USB Configs */
#ifdef CONFIG_CMD_USB
#define CONFIG_EHCI_HCD_INIT_AFTER_RESET
#define CONFIG_USB_HOST_ETHER
#define CONFIG_USB_ETHER_ASIX
#define CONFIG_MXC_USB_PORTSC		(PORT_PTS_UTMI | PORT_PTS_PTW)
#define CONFIG_MXC_USB_FLAGS		0
#define CONFIG_USB_MAX_CONTROLLER_COUNT	2 /* Enabled USB controller number */
#endif

#endif                         /* __Q7M120_CONFIG_H */
